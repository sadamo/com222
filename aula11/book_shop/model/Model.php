<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Model {
    public function getBookList() {
        $books = BookDB::getBookList();
        return $books;
    }
    public function getBookByTitle($title) {
        $book = BookDB::getBookByTitle($title);
        return $book;
    }
}
