<?php

class BookDB {
    public static function getBookList() {
        $db = Database::getDB();
        $query = 'SELECT * FROM livro ORDER BY codigo';
        $statememt = $db->query($query);
        $statememt->execute();
        
        $allBooks = array();
        foreach ($statememt as $row) {
            $book = new Book($row['titulo'], $row['autor'], $row['descricao']);
            $allBooks[] = $book;
        }
        return $allBooks;
    }

    public static function getBookByTitle($title) {
        $db = Database::getDB();
        $query = 'SELECT * FROM livro
                     WHERE livro.titulo = :title';
        $statement = $db->prepare($query);
        $statement->bindValue(":title", $title);
        $statement->execute();
        $row = $statement->fetch();
        $statement->closeCursor();
       
        $book = new Book($row['titulo'], $row['autor'], $row['descricao']);

        return $book;
    }

    public static function addBook($book) {
        $db = Database::getDB();

        $titulo = $book->getTitulo();
        $autor = $book->getAutor();
        $descricao = $book->getDescricao();

        $query = 'INSERT INTO livro
                     (titulo, autor, descricao)
                  VALUES
                     (:titulo, :autor, :descricao)';
        $statement = $db->prepare($query);
        $statement->bindValue(':titulo', $titulo);
        $statement->bindValue(':autor', $autor);
        $statement->bindValue(':descricao', $descricao);
        $statement->execute();
        $statement->closeCursor();
    }

}
