<?php include '../view/header.php'; ?>
<main>
    <h1>Add Category</h1>
    <form action="index.php" method="post" id="add_category_form">
        <input type="hidden" name="action" value="add_category">

        <label>Category:</label>

        <label>Name:</label>
        <input type="text" name="categoryName" />
        <br>
        <br>

        <label>&nbsp;</label>
        <input type="submit" value="Add Category" />
        <br>
        <br>
    </form>
    <p class="last_paragraph">
        <a href="?action=list_products">View Product List</a>
    </p>
</main>
<?php include '../view/footer.php'; ?>