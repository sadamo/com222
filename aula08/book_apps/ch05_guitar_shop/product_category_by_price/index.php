<?php
require('../model/database.php');
require('../model/product_db.php');
require('../model/category_db.php');

$action = filter_input(INPUT_POST, 'action');
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == NULL) {
        $action = 'list_products_by_price';
    }
}
// Create a cart array if needed
if (empty($_SESSION['cart'])) { $_SESSION['cart'] = array(); }

if ($action == 'list_products_by_price'){
    /*
     * 0 até 299.99 price_range=0
     * de 300.00 a 599.99 price_range=1
     * de 600.00 a 999.99 price_range=2
     * acima de 1,000.00 price_range=3
     *
    */
    $price_range = filter_input(INPUT_GET, 'price_range',
        FILTER_VALIDATE_INT);
    if ($price_range == NULL) {
        $price_range = 0;
        $category_range = "Até 299.99";
    }

    if($price_range == 0){
        $products = get_products_by_price(0, 299.99);
        $category_range = "Até 299.99";
    } elseif ($price_range == 1){
        $products = get_products_by_price(300, 599.99);
        $category_range = "De 300.00 a 599.99";
    } elseif ($price_range == 2){
        $products = get_products_by_price(600, 999.99);
        $category_range = "De 600.00 a 999.99";
    } elseif ($price_range == 3){
        $products = get_products_by_price(1000, 10000);
        $category_range = "Acima de 1,000.00";
    }
    include('product_list_by_price.php');
}