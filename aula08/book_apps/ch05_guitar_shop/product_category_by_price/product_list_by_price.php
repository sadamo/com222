<?php include '../view/header.php'; ?>
    <main>
        <aside>
            <h1>Categories</h1>
            <nav>
                <ul>
                    <!-- display links for all ranges -->
                    <li>
                        <a href="?price_range=0">
                            Até 299.99
                        </a><br>
                    </li>
                    <li>
                        <a href="?price_range=1">
                           300.00 até 599.99
                        </a><br>
                    </li>
                    <li>
                        <a href="?price_range=2">
                            600.00 até 999.99
                        </a><br>
                    </li>
                    <li>
                        <a href="?price_range=3">
                            Acima de 1,000.00
                        </a><br>
                    </li>
                </ul>
            </nav>
        </aside>
        <section>
            <h1><?php echo $category_range; ?></h1>

            <table style="border: hidden;">
                <tr>
                    <th>Name</th>
                    <th class="right">Price</th>
                    <th>&nbsp;</th>
                </tr>
                <?php foreach ($products as $product) : ?>
                    <tr>
                        <td><?php echo $product['productName']; ?></td>
                        <td class="right"><?php echo $product['listPrice']; ?></td>
                        <td><a href="../product_catalog/?action=view_product&amp;product_id=<?php
                            echo $product['productID']; ?>">Ver</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </section>

            <h4>
                <a href="../cart">Ver Carrinho</a>
            </h4>

    </main>
<?php include '../view/footer.php'; ?>