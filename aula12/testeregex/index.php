<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <p>
        <h1>Exercicio 1</h1>
        <h3>
            Como validar um número de telefone com 8 ou 9 dígitos,
            com ou sem hífem?
        </h3>
        <?php
        // put your code here
        $string = '12345678';
        echo "string: " . $string . "<br>";
        echo "regex: ^[0-9]?[0-9]{4}\-?[0-9]{4}$ <br>";
        echo "resultado: " . preg_match("/^[0-9]?[0-9]{4}\-?[0-9]{4}$/", $string, $matches) . "<br>";
        ?>
    </p>
    <p>
    <h1>Exercicio 2</h1>
    <form action="." method="post" >
        <fieldset>
            <legend>Escreva um programa em PHP que contenha um
                formulário, com os seguintes campos:
                CPF, Nome, Sobrenome, telefone, email e data de nasc.
                Utilizando expressões regulares, faça a validação dos
                campos:</legend>

            <label>CPF:</label>
            <input type="text" name="cpf" 
                   placeholder="xxx.xxx.xxx-xx">
            <br>

            <label>Email:</label>
            <input type="text" name="mail" 
                   placeholder="xx...xxx@xxx...xxx">
            <br>

            <label>Número de telefone:</label>
            <input type="text" name="phone" 
                   placeholder="(xx) xxxx xxxx">
            <br>

            <label>Datas no formato brasileiro:</label>
            <input type="text" name="datas" 
                   placeholder="dd/mm/aaaa">
            <br>
        </fieldset>
        <fieldset>
            <legend>Submit Registration</legend>

            <label>&nbsp;</label>
            <input type="submit" name="action" value="Register"/>
            <input type="submit" name="action" value="Reset" /><br>
        </fieldset>
    </form>
    <?php
    $action = filter_input(INPUT_POST, 'action');
    if ($action === NULL) {
        $action = 'reset';
    } else {
        $action = strtolower($action);
    }

    switch ($action) {
        case 'reset':
            // Reset values for variables
            $cpf = '';
            $mail = '';
            $phone = '';
            $datas = '';
            // Load view
            
            break;
        case 'register':
            // Copy form values to local variables
            $cpf = trim(filter_input(INPUT_POST, 'cpf'));
            $mail = trim(filter_input(INPUT_POST, 'mail'));
            $phone = trim(filter_input(INPUT_POST, 'phone'));
            $datas = trim(filter_input(INPUT_POST, 'datas'));

            // Validate form data
            //validação cpf
            echo "<fieldset>";
            $pattern_cpf = '/^\d{3}(.\d{3}){2}-\d{2}$/';
            $match = preg_match($pattern_cpf, $cpf);
            if ($match) {
                echo "<p>Campo CPF validado com sucesso: ".$cpf."</p>";
            } else {
                echo "Campo CPF inválido<br>";
            }

            
            $pattern_mail = '/^[[:alnum:]]{1,}@[[:alnum:]]{1,}.[[:alnum:]]{1,}(.[[:alnum:]]{1,})?$/';
            $match = preg_match($pattern_mail, $mail);
            if ($match) {
                echo "<p>Campo Email validado com sucesso ".$mail."</p>";
            } else {
                echo "Campo Email inválido<br>";
            }
            
            $pattern_phone = '/^\(\d{2}\) \d{4} \d{4}$/';
            $match = preg_match($pattern_phone, $phone);
            if ($match) {
                echo "<p>Campo Telefone validado com sucesso ".$phone."</p>";
            } else {
                echo "Campo Telefone inválido<br>";
            }
            
            $pattern_datas = '/^([0-2][0-9]|[3][01])\/([0]\d|[1][0-2])\/\d{4}$/';
            $match = preg_match($pattern_datas, $datas);
            if ($match) {
                echo "<p>Campo Data validado com sucesso ".$datas."</p>";
            } else {
                echo "Campo Data inválido<br>";
            }
            echo "</fieldset>";
            
            break;
    }
    ?>

</body>
</html>
