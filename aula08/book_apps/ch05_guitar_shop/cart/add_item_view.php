<?php include '../view/header.php'; ?>
<main>
    <h1>Add Item</h1>
    <form action="." method="post">
        <input type="hidden" name="action" value="add">

        <label>Name:</label>
        <select name="product_id">
            <?php foreach($products as $product) :?>
                <option value="<?php echo $product['productID']; ?>">
                    <?php echo $product['productName']; ?>
                    <?php echo $product['listPrice']; ?>
                </option>
            <?php endforeach; ?>
        </select>
        <br>

        <label>Quantity:</label>
        <select name="item_qty">
            <?php for($i = 1; $i <= 10; $i++) : ?>
                <option value="<?php echo $i; ?>">
                    <?php echo $i; ?>
                </option>
            <?php endfor; ?>
        </select>
        <br>
        <br>
        <label>&nbsp;</label>
        <input type="submit" value="Add Item">
    </form>
    <p><a href=".?action=show_cart">View Cart</a></p>
</main>
<?php include '../view/footer.php'; ?>