<?php
require('model/Database.php');
require('model/Model.php');
require('model/Book.php');
require('model/BookDB.php');

class Controller {
    public $model;
    
    public function __construct() {
        $this->model = new Model();
    }
    
    public function invoke() {

        $action = filter_input(INPUT_POST, 'action');
        if ($action == NULL) {
            $action = filter_input(INPUT_GET, 'action');
            if ($action == NULL) {
                $action = 'viewbooklist';
            }
        }
        if($action == 'viewbooklist') { 
            // nenhum livro foi requisitado,
            // mostrar lista de todos os livros disponíveis
            $books = $this->model->getBookList();
            include 'view/booklist.php';

        } else if ($action == 'viewbook') {
            // mostra o livro requisitado
            $book = $this->model->getBookByTitle($_GET['book']);
            include 'view/viewbook.php';

        } else if ($action == 'show_add_form') {
            // mostra formulario para adicionar novo livro
            include 'view/book_add_form.php';

        } else if ($action == 'book_add') {
            echo "Adicionando produto";
            $titulo = filter_input(INPUT_POST, 'titulo');
            $autor = filter_input(INPUT_POST, 'autor');
            $descricao = filter_input(INPUT_POST, 'descricao');

            $book = new Book($titulo, $autor, $descricao);
            BookDB::addBook($book);
            include 'view/viewbook.php';
        } 

    }
}