<html>
<head>
    <title>Consulta alunos</title>
</head>
<body>
<div align="center">
<h1> Banco de dados de alunos</h1>
<p> Ordenar a lista de estudantes por
    <a href="data_out.php?ordem=matric">matricula</a>,
    <a href="data_out.php?ordem=nome">nome</a>, ou
    por <a href="data_out.php?ordem=email">e-mail</a>.
</p>
<?php
/* obtem alunos do banco */
$db = new mysqli("172.20.0.2", "root", 'docker', 'escola', '3306');
if (!isset($_GET["ordem"])) {
    $sql = "SELECT * FROM aluno";
} else {
    switch ($_GET["ordem"]) {
        case 'matric':
            $sql = "SELECT * FROM aluno ORDER BY matric";
            break;
        case 'nome':
            $sql = "SELECT * FROM aluno ORDER BY nome";
            break;
        case 'email':
            $sql = "SELECT * FROM aluno ORDER BY email";
            break;
        default:
            $sql = "SELECT * FROM aluno";
            break;
    }
}
//echo $sql;
$result = $db->query($sql); /* executa a query */

while ($row = $result->fetch_assoc()) {
    echo "<h4> Nome: " . $row["nome"] . "</h4> \n";
    echo "<h5> Matricula: " . $row["matric"] . "<br/> Email: " . $row["email"] . "<br/>
    Endereco: " . $row["endereco"] . "</h5> \n";
}
$db->close();
?>
<br>
<br>
<h4><a href="index.php">Inicio</a></h4>
</div>
</body>
</html>