<html>
<head>
    <title>Gestão de Alunos</title>
</head>
<body>
    <div align="center">
        <h2>Gestão de Alunos</h2>
        <ul style="list-style-type: none; padding: 0px;">
            <li><a href="data_in.php">Inserir estudante</a></li>
            <li><a href="data_out.php">Consultar lista de estudantes</a></li>
            <li><a href="data_find.php">Consultar estudante</a></li>
            <li><a href="data_update.php">Atualizar estudante</a></li>
            <li><a href="data_remove.php">Remover estudante</a></li>
        </ul>
    </div>
</body>
</html>