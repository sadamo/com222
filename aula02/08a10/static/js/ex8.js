function ex8() {
    var data_curto = prompt("Insira a data no formato curto ", "DD/MM/AAA");
    var fat = data_curto.split("/");
    var ano = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 
                'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];


    var data = new Date(fat[2], fat[1], fat[0]);
    console.log("Data:" + data.getDate() + " de " + ano[data.getMonth()-1] + " de " + data.getFullYear());

    document.getElementById("resultado08").innerHTML = "<p>Data: " + data.getDate() + " de " + ano[data.getMonth()-1] + " de " + data.getFullYear() + "</p>";
}