<?php
// Add an item to the cart
function add_item($product_id, $quantity) {
    $product = get_product($product_id);
    if ($quantity < 1) return;

    // If item already exists in cart, update quantity
    if (isset($_SESSION['cart'][$product_id])) {
        $quantity += $_SESSION['cart'][$product_id]['qty'];
        update_item($product_id, $quantity);
        return;
    }

    // Add item
    $cost = $product['listPrice'];
    $total = $cost * $quantity;
    $item = array(
        'name' => $product['productName'],
        'cost' => $cost,
        'qty'  => $quantity,
        'total' => $total
    );
    $_SESSION['cart'][$product_id] = $item;
}

// Update an item in the cart
function update_item($product_id, $quantity) {
    $quantity = (int) $quantity;
    if (isset($_SESSION['cart'][$product_id])) {
        if ($quantity <= 0) {
            unset($_SESSION['cart'][$product_id]);
        } else {
            $_SESSION['cart'][$product_id]['qty'] = $quantity;
            $total = $_SESSION['cart'][$product_id]['cost'] *
                $_SESSION['cart'][$product_id]['qty'];
            $_SESSION['cart'][$product_id]['total'] = $total;
        }
    }
}

// Get cart subtotal
function get_subtotal() {
    $subtotal = 0;
    foreach ($_SESSION['cart'] as $item) {
        $subtotal += $item['total'];
    }
    $subtotal_f = number_format($subtotal, 2);
    return $subtotal_f;
}
?>