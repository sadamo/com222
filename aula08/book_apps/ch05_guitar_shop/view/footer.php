<footer>
    <p><h4 style="margin: 6px;"><a href="<?php echo $_SERVER['HTTP_REFERER']; ?>"><< Back</a></p>
    <p class="copyright">
        &copy; <?php echo date("Y"); ?> My Guitar Shop, Inc.
    </p>
</footer>
</body>
</html>