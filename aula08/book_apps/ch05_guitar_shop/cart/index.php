<?php
require('../model/database.php');
require('../model/product_db.php');
require('../model/category_db.php');

// Start session management with a persistent cookie
$lifetime = 60 * 60 * 24 * 14;    // 2 weeks in seconds
session_set_cookie_params($lifetime, '/');
session_start();

// Create a cart array if needed
if (empty($_SESSION['cart'])) { $_SESSION['cart'] = array(); }

// Create a table of products

// Include cart functions
require_once('cart.php');

// Get the action to perform
$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'show_cart';
    }
}

// Add or update cart as needed
switch($action) {
    case 'add':
        $product_id = filter_input(INPUT_POST, 'product_id');
        $item_qty = filter_input(INPUT_POST, 'item_qty');
        echo "item qnt: " . $item_qty . " /n ";
//        echo "prod ID " .  $_SESSION['cart'][$product_id] . " /n ";
        echo "prodID: " .$product_id . " /n ";
        $product = get_product($product_id);
        echo $product['productID'];
        add_item($product_id, $item_qty) . " --- ";
        include('cart_view.php');
        break;
    case 'update':
        $new_qty_list = filter_input(INPUT_POST, 'newqty', FILTER_DEFAULT,
            FILTER_REQUIRE_ARRAY);
        foreach($new_qty_list as $key => $qty) {
            if ($_SESSION['cart'][$key]['qty'] != $qty) {
                update_item($key, $qty);
            }
        }
        include('cart_view.php');
        break;
    case 'show_cart':
        include('cart_view.php');
        break;
    case 'show_add_item':
        $products = get_products();
        include('add_item_view.php');
        break;
    case 'empty_cart':
        unset($_SESSION['cart']);
        include('cart_view.php');
        break;
}
?>