<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Book {
    public $titulo, $autor, $descricao;
    
    public function __construct($title, $author, $description) {
        $this->titulo = $title;
        $this->autor = $author;
        $this->descricao = $description;
    }

    public function getTitulo(){
        return $this->titulo;
    }
    public function getAutor(){
        return $this->autor;
    }
    public function getDescricao(){
        return $this->descricao;
    }
}

