function ex6() {
    var primeiro_num = parseInt(prompt("Insira o primeiro numero: ", "0"));
    var segundo_num = parseInt(prompt("Insira o segundo numero: ", "0"));

    if (primeiro_num === 0 || segundo_num === 0) {
        alert("Insira numeros diferentes de zero");
        return;
    }
    var resSoma = primeiro_num + segundo_num;
    var resMult = primeiro_num * segundo_num;
    var resDiv = primeiro_num / segundo_num;
    var resto = primeiro_num % segundo_num;
    document.getElementById("tabela").innerHTML = " ";
    document.getElementById("tabela").innerHTML += "<tr><td>" + primeiro_num + " + " + segundo_num + " </td><td>" + resSoma +  " </td></tr>";
    document.getElementById("tabela").innerHTML += "<tr><td>" + primeiro_num + " * " + segundo_num + " </td><td>" + resMult +  " </td></tr>";
    document.getElementById("tabela").innerHTML += "<tr><td>" + primeiro_num + " / " + segundo_num + " </td><td>" + resDiv +  " </td></tr>";
    document.getElementById("tabela").innerHTML += "<tr><td>" + primeiro_num + " % " + segundo_num + " </td><td>" + resto +  " </td></tr>";
}