<html>
<head>
    <title>Removendo dados no banco</title>
</head>
<body>
<div align="center">
    <?php
    /* insere alunos no banco */
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $db = new mysqli('172.20.0.2', 'root', 'docker', 'escola', '3306');

        if ($db->connect_errno) {
            echo "Erro na conexão com o banco de dados: " . $db->connect_error;
            exit();
        }

        $sql = "SELECT * FROM aluno WHERE matric = " . $_POST["matric"] . ";";
        //echo $sql;

        if (!$db->query($sql))
            echo "Erro na execução da query";

        $result = $db->query($sql);

        while ($row = $result->fetch_assoc()) {
            echo "<h4> Nome: " .$row["nome"] . "</h4>\n";
            echo "<h4> Matricula: " .$row["matric"] . "</h4>\n";
            echo "<h3> Removido com sucesso!</h3>\n";
        }
        $db->close();
        echo '<p><a href="data_out.php">Veja a lista de alunos</a></p>' . "\n";
    } else {
        ?>
        <h3>Entre com os dados:</h3>
        <form action="data_remove.php" method="post">
            Matricula: <input type="text" name="matric"/> <br/>
            <br/>
            <input type="submit" name="submit"/> <input type="reset"/>
        </form>
    <?php } ?>
    <br>
    <br>
    <h4><a href="index.php">Inicio</a></h4>
</div>
</body>
</html>